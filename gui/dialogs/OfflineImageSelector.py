import wx

class OfflineImageSelector(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent)
		
		self.Sizer = wx.BoxSizer(wx.VERTICAL)
		self.Caption = wx.StaticText(self, label="Select Offline Images")
		self.Sizer.Add(self.Caption, 1, wx.EXPAND)
		self.ImageSelector = wx.ListBox(self)
		self.Sizer.Add(self.ImageSelector, 1, wx.EXPAND)
		
		self.BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.BtnSizer.AddStretchSpacer(1)
		self.ConfirmBtn = wx.Button(self, label="Confirm")
		self.ConfirmBtn.Bind(wx.EVT_BUTTON, self.onConfirm)
		self.BtnSizer.Add(self.ConfirmBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.CancelBtn = wx.Button(self, label="Cancel")
		self.CancelBtn.Bind(wx.EVT_BUTTON, self.onCancel)
		self.BtnSizer.Add(self.CancelBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.Sizer.Add(self.BtnSizer, 1, wx.EXPAND)
		
		self.SetSizer(self.Sizer)
		
	def onConfirm(self, evt):
		if self.IsModal():
			if self.ImageSelector.GetSelection() > 0:
				self.EndModal(wx.ID_OK)
		else:
			self.Close()
	def onCancel(self, evt):
		if self.IsModal():
			self.EndModal(wx.ID_CANCEL)
		else:
			self.Close()
