import wx


class AddReactionDialog(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent)
		
		self.ReactionSizer = wx.BoxSizer(wx.VERTICAL)
		self.ReactionSizer.AddStretchSpacer(1)
		self.Caption = wx.StaticText(self, label="Enter Reaction Name:")
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.Caption.SetFont(self.CaptionFont)
		self.ReactionSizer.Add(self.Caption, 1, wx.ALIGN_CENTER)
		self.ReactionSizer.AddStretchSpacer(1)
		self.Text = wx.TextCtrl(self, value="")
		self.ReactionSizer.Add(self.Text, 2, wx.ALIGN_CENTER | wx.EXPAND)
		self.ReactionSizer.AddStretchSpacer(1)
		
		self.BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.BtnSizer.AddStretchSpacer(1)
		self.ConfirmBtn = wx.Button(self, label="Confirm")
		self.ConfirmBtn.Bind(wx.EVT_BUTTON, self.onConfirm)
		self.BtnSizer.Add(self.ConfirmBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.CancelBtn = wx.Button(self, label="Cancel")
		self.CancelBtn.Bind(wx.EVT_BUTTON, self.onCancel)
		self.BtnSizer.Add(self.CancelBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.ReactionSizer.Add(self.BtnSizer, 1, wx.EXPAND)
		self.ReactionSizer.AddStretchSpacer(1)
		
		self.SetSizer(self.ReactionSizer)
		
	def onConfirm(self, evt):
		if self.IsModal():
			if self.Text.GetValue() != "":
				self.EndModal(wx.ID_OK)
		else:
			self.Close()
	def onCancel(self, evt):
		if self.IsModal():
			self.EndModal(wx.ID_CANCEL)
		else:
			self.Close()
