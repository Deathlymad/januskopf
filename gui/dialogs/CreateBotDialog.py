import wx
import wx.html
import os.path

import util.RessourcePath
import webbrowser

class LinkWrapper(wx.html.HtmlWindow):
	def OnLinkClicked(self, link):
		webbrowser.open(link.GetHref());

class BotCreateDialog(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent, title="Create Bot", pos=(300,300), size=(800,600))
		
		self.HeadSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.SetSizer(self.HeadSizer)
		
		self.HeadSizer.AddStretchSpacer(4)
		self.TextSizer = wx.BoxSizer(wx.VERTICAL)
		self.TextSizer.AddStretchSpacer(2)
		self.Caption = wx.StaticText(self, label="Bot\nCreation", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE);
		self.CaptionFont = wx.Font(20, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.Caption.SetFont(self.CaptionFont)
		self.TextSizer.Add(self.Caption, 4, wx.EXPAND)
		self.TextSizer.AddStretchSpacer(1)
		self.Hints = LinkWrapper(self)
		self.Hints.SetPage("""
		<ul>
		<li>First, enter a Bot Token:
		<ol type="1">
			<li>Go to the <a href=https://discordapp.com/developers/applications/me>Discord Dev Website</a> and add a new App.</li>
			<li>Define the Application as a Bot Account while clicking on "Create Bot User".</li>
			<li>Now you can reveal a Token on the Bot Panel. Copy it in the Text Box labeled "Discord Bot Token".</li>
		</ol></li>
		<li>The Name does not have to be the exact Name of the Bot. Any Alias works, as it is mainly used internally.</li>
		<li>Since the Command Prefix for the Bot is based on the Name, it is however recommended.</li>
		<li>The TalkFile is a JSON file that contains the Information of all Interactions the Bot is capable of doing. Select a Location to store it.</li>
		<li>The Image Search Path is for looking up Images stored offline that can be uploaded in conjunction with reactions.</li>
		</ul>
		""")
		self.TextSizer.Add(self.Hints, 40, wx.EXPAND)
		self.TextSizer.AddStretchSpacer(2)
		
		self.HeadSizer.Add(self.TextSizer, 40, wx.EXPAND | wx.ALIGN_LEFT)
		self.HeadSizer.AddStretchSpacer(4)
		
		self.EditSizer = wx.BoxSizer(wx.VERTICAL)

		self.EditSizer.AddStretchSpacer(1)
		self.TokenCaption = wx.StaticText(self, label="Discord Bot Token:", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.EditSizer.Add(self.TokenCaption, 2, wx.ALIGN_LEFT)
		self.TokenBox = wx.TextCtrl(self, value="")
		self.EditSizer.Add(self.TokenBox, 4, wx.EXPAND)
		
		self.EditSizer.AddStretchSpacer(1)
		self.NameCaption = wx.StaticText(self, label="Name:")
		self.EditSizer.Add(self.NameCaption, 2, wx.ALIGN_LEFT)
		self.NameBox = wx.TextCtrl(self, value="")
		self.EditSizer.Add(self.NameBox, 4, wx.EXPAND)
		
		self.EditSizer.AddStretchSpacer(1)
		self.TalkFileCaption = wx.StaticText(self, label="Reaction File", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.EditSizer.Add(self.TalkFileCaption, 2, wx.ALIGN_LEFT)
		self.TalkFilePath = wx.TextCtrl(self, value="")
		self.EditSizer.Add(self.TalkFilePath, 4, wx.EXPAND)
		self.EditSizer.AddStretchSpacer(1)
		self.TalkFileSearchBtn = wx.Button(self, label="Talk File Location", style=wx.ST_NO_AUTORESIZE)
		self.TalkFileSearchBtn.Bind(wx.EVT_BUTTON, self.selectTalkFile)
		self.EditSizer.Add(self.TalkFileSearchBtn, 1, wx.EXPAND | wx.CENTER)
		
		self.EditSizer.AddStretchSpacer(1)
		self.ImgPathCaption = wx.StaticText(self, label="Root Image File Path:", style=wx.ST_NO_AUTORESIZE)
		self.EditSizer.Add(self.ImgPathCaption, 2, wx.ALIGN_LEFT)
		self.ImgPath = wx.TextCtrl(self, value="")
		self.EditSizer.Add(self.ImgPath, 4, wx.EXPAND)
		self.EditSizer.AddStretchSpacer(1)
		self.ImgPathSearchBtn = wx.Button(self, label="Select Directory", style=wx.ST_NO_AUTORESIZE)
		self.ImgPathSearchBtn.Bind(wx.EVT_BUTTON, self.selectImgPath)
		self.EditSizer.Add(self.ImgPathSearchBtn, 1, wx.EXPAND | wx.CENTER)
		
		self.CloseButton = wx.Button(self, label="Save & Quit")
		self.CloseButton.Bind(wx.EVT_BUTTON, self.onSaveAndQuit)
		self.EditSizer.AddStretchSpacer(1)
		self.EditSizer.Add(self.CloseButton, 3, wx.EXPAND | wx.CENTER)
		
		self.EditSizer.AddStretchSpacer(2)
		self.HeadSizer.Add(self.EditSizer, 60, wx.EXPAND | wx.ALIGN_RIGHT)
		self.HeadSizer.AddStretchSpacer(4)
		
		self.SetSizer(self.HeadSizer)

	def selectTalkFile(self, evt):
		fileDlg = wx.FileDialog( self, "Choose Talk File", style=wx.FD_OPEN)
		fileDlg.ShowModal()
		self.TalkFilePath.SetValue(os.path.abspath(fileDlg.GetPath()))
		fileDlg.Destroy()
	
	def selectImgPath(self, evt):
		dirDlg = wx.DirDialog( self, "Choose Image Directory", style=wx.DD_DIR_MUST_EXIST)
		dirDlg.ShowModal()
		self.ImgPath.SetValue(os.path.abspath(dirDlg.GetPath()))
		dirDlg.Destroy()
	
	def onSaveAndQuit(self, evt):
		if (self.isValid()):
			self.EndModal(0)
		else:
			self.EndModal(1)
	
	def getBotName(self):
		return self.NameBox.GetValue()
	def getData(self):
		return {self.NameBox.GetValue() : {"token" : self.TokenBox.GetValue(), "talkFile" : self.TalkFilePath.GetValue(), "localImagePath" : self.ImgPath.GetValue()}}
	def isValid(self):
		return self.TokenBox.GetValue() != "" and self.NameBox.GetValue() != "" and self.TalkFilePath.GetValue() != "" and self.ImgPath.GetValue() != ""
