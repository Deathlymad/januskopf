import wx


class AddFriendDialog(wx.Dialog):
	def __init__(self, parent, friends):
		wx.Dialog.__init__(self, parent)
		
		self.Sizer = wx.BoxSizer(wx.VERTICAL)
		self.Caption = wx.StaticText(self, label="Select Friend:", style=wx.ALIGN_CENTER)
		self.Caption.SetFont(wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.Sizer.Add(self.Caption, 1, wx.EXPAND)
		self.List = wx.ListBox(self, choices = friends)
		self.Sizer.Add(self.List, 1, wx.EXPAND)
		
		self.BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.BtnSizer.AddStretchSpacer(1)
		self.ConfirmBtn = wx.Button(self, label="Confirm")
		self.ConfirmBtn.Bind(wx.EVT_BUTTON, self.onConfirm)
		self.BtnSizer.Add(self.ConfirmBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.CancelBtn = wx.Button(self, label="Cancel")
		self.CancelBtn.Bind(wx.EVT_BUTTON, self.onCancel)
		self.BtnSizer.Add(self.CancelBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.Sizer.Add(self.BtnSizer, 1, wx.EXPAND)
		
		self.SetSizer(self.Sizer)

	def onConfirm(self, evt):
		if self.IsModal():
			if not self.List.GetSelection() == wx. NOT_FOUND:
				self.EndModal(wx.ID_OK)
		else:
			self.Close()
	def onCancel(self, evt):
		if self.IsModal():
			self.EndModal(wx.ID_CANCEL)
		else:
			self.Close()
