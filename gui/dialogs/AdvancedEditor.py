import wx
from util.HighlightTextCtrl import HighlightTextCtrl

class AdvancedEditor(wx.Dialog):
	def __init__(self, parent, text, quotes=None):
		wx.Dialog.__init__(self, parent, pos=(300,300), size=(800,600))
		
		self.HeadSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.HeadSizer.AddStretchSpacer(1)
		self.EditSizer = wx.BoxSizer(wx.VERTICAL)
		self.Caption = wx.StaticText(self, label="Advanced Reaction Editor", style=wx.ALIGN_CENTER)
		self.Caption.SetFont(wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.EditSizer.AddStretchSpacer(1)
		self.EditSizer.Add(self.Caption, 1, wx.EXPAND)
		self.EditSizer.AddStretchSpacer(1)
		self.Editor = HighlightTextCtrl(self, text)
		self.EditSizer.Add(self.Editor, 20, wx.EXPAND)
		self.EditSizer.AddStretchSpacer(1)
		self.HeadSizer.Add(self.EditSizer, 5, wx.EXPAND)
		
		self.LookupList = wx.ListBox(self)
		self.HeadSizer.AddStretchSpacer(1)
		self.HeadSizer.Add(self.LookupList, 4, wx.EXPAND)
		self.HeadSizer.AddStretchSpacer(1)
		self.SetSizer(self.HeadSizer)
		
		if quotes != None:
			for key, value in quotes.items():
				self.LookupList.Append("q{" + key + "};")

	
	def onDblClick(self, evt):
		self.Editor.Append(self.LookupList.GetString(self.LookupList.GetSelection()))
		evt.Skip()