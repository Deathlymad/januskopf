import wx


class AddReferenceDialog(wx.Dialog):
	def __init__(self, parent, value=None):
		wx.Dialog.__init__(self, parent)
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		
		self.Sizer = wx.BoxSizer(wx.VERTICAL)
		self.NameCaption = wx.StaticText(self, label="Name of Reference", style=wx.ALIGN_CENTER)
		self.NameCaption.SetFont(self.CaptionFont)
		self.Sizer.Add(self.NameCaption, 1, wx.EXPAND)
		self.NameText = wx.TextCtrl(self)
		self.Sizer.Add(self.NameText, 1, wx.EXPAND)
		self.StateCaption = wx.StaticText(self, label="Value of Reference", style=wx.ALIGN_CENTER)
		self.StateCaption.SetFont(self.CaptionFont)
		self.Sizer.Add(self.StateCaption, 1, wx.EXPAND)
		self.RefText = wx.TextCtrl(self)
		self.Sizer.Add(self.RefText, 1, wx.EXPAND)
		
		self.BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.BtnSizer.AddStretchSpacer(1)
		self.ConfirmBtn = wx.Button(self, label="Confirm")
		self.ConfirmBtn.Bind(wx.EVT_BUTTON, self.onConfirm)
		self.BtnSizer.Add(self.ConfirmBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.CancelBtn = wx.Button(self, label="Cancel")
		self.CancelBtn.Bind(wx.EVT_BUTTON, self.onCancel)
		self.BtnSizer.Add(self.CancelBtn, 2, wx.ALIGN_CENTER)
		self.BtnSizer.AddStretchSpacer(1)
		self.Sizer.Add(self.BtnSizer, 1, wx.EXPAND)
		
		self.SetSizer(self.Sizer)
		
		if value:
			self.NameText.SetValue(value["name"])
			self.RefText.SetValue(value["value"])
		
	def onConfirm(self, evt):
		if self.IsModal():
			if self.NameText.GetValue() != "" and self.RefText.GetValue() != "":
				self.EndModal(wx.ID_OK)
		else:
			self.Close()
	def onCancel(self, evt):
		if self.IsModal():
			self.EndModal(wx.ID_CANCEL)
		else:
			self.Close()
