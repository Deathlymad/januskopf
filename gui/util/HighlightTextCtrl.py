import wx
import re


class HighlightTextCtrl(wx.TextCtrl):
	def __init__(self, parent, text):
		wx.TextCtrl.__init__(self, parent, value=text, style=wx.TE_MULTILINE | wx.TE_RICH2 | wx.TE_PROCESS_ENTER)
		self.QuoteRegex = re.compile(r"(q{[^\}\s]+};)")
		self.VariableRegex = re.compile(r"([v=]{[^\}\s]+};)")
		self.ConditionRegex = re.compile(r"c{(.+)\?(.+)\:(.+)};")
		self.Bind(wx.EVT_TEXT_ENTER, self.onEdit)
	
	def onEdit(self, evt): #event trigger might be tricky
		for condition in self.ConditionRegex.finditer(self.GetValue()):
			self.SetStyle(condition.start(), condition.end(), wx.TextAttr(wx.Colour(20, 200, 200), wx.Colour(200, 20, 20, 100), wx.Font(10, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)))
		for condition in self.VariableRegex.finditer(self.GetValue()):
			self.SetStyle(condition.start(), condition.end(), wx.TextAttr(wx.Colour(20, 200, 20)))
		for condition in self.QuoteRegex.finditer(self.GetValue()):
			self.SetStyle(condition.start(), condition.end(), wx.TextAttr(wx.Colour(20, 20, 200)))

	def SetValue(self, value):
		wx.TextCtrl.SetValue(self, value)
		self.onEdit(None)