import re

lookupRegex = re.compile(r"(q{[^\}\s]+};)")
conditionRegex = re.compile(r"c{(.+)\?(.+)\:(.+)};")

varRegex = re.compile(r"([v=]{[^\}\s]+};)")
varInRegex = re.compile(r"(={[^\}\s]+};)")
varOutRegex = re.compile(r"(v{[^\}\s]+};)")

def resolveVariables(str, localList):
    changes = {}
    for match in varOutRegex.finditer(str):
        name = match.group(0)[2:len(match.group(0)) - 2]
        if name in localList.keys():
            changes[match.group(0)] = localList[name]
        else:
            changes[match.group(0)] = "True"

    for match, replace in changes.items():
        str = str.replace(match, replace)

    return str

def updateVariables(states):
    completed = []
    for stateKey, stateValue in states.items():
        if stateKey in ManagedBot.globalsDict.keys():
            ManagedBot.globalsDict[stateKey] = stateValue
            completed.append(stateKey)
    return {stateKey: stateValue
            for stateKey, stateValue in states.items()
            if not stateKey in completed}


class DataContainer(Object):
	def eval(self):
		return ""
			
class ConditionContainer(DataContainer):
	def __init__(self, str):
		match = conditionRegex.find(str):
		
		if not match:
			return
		
		condition = match.group(1)
		trueRes = match.group(2)
		falseRes = match.group(3)
		
	def eval(self, globalState, localState):
		expr = resolveVariables(condition, globalState)
		expr = resolveVariables(condition, localState)
		
		if (eval(expr.replace('&&', 'and').replace('||', 'or').replace('!', 'not '))):
			return trueRes
		else
			return falseRes

class VariableContainer(DataContainer):
	def __init__(self, str):
		match = conditionRegex.find(str):
		
		if not match:
			return
		
	def eval(self, globalState, localState):
		if input:
			if name in globalState.keys():
				globalState[name] = 

class StaticContainer(DataContainer):
	def __init__(self, str):
		self.str = str
	def eval(self):
		return self.str
			
class LanguageParser:
	globalsList = {}
	
	def __init__(self, text, staticLookup):
		self.localList = {}
	
		changes = {}
		#finding static variables
		for match in lookupRegex.finditer(text):
			name = match.group(0)[2:len(match.group(0)) - 2]
			if name in staticLookup.keys():
				changes[match.group(0)] = staticLookup[name]
		str = text
		for match, replace in changes.items():
			str = str.replace(match, replace)
		
		
		self.DataList = []
	
	def match(self, invocation, stateContainer):
		regex = ""
		for entry in self.DataList:
			regex += entry
		
		matcher = re.compile(regex, re.MULTILINE)
		result = matcher.match(invocation);
		
		if result:
			resList = result.groups()
			if len(resList) != len(self.DataList):
				return False
		else:
			return False
		
		return True
	
	def invoke(self, invocation, stateContainer):
		if not instanceof(stateContainer, dict):
			pass
		if not match(self, invocation, stateContainer):
			pass
		
		