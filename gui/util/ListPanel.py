import wx

class ListPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.Bind(wx.EVT_SIZE, self.onResize)
		
		self.Caption = wx.StaticText(self, label="List:", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.List = wx.ListBox(self, choices=[], style=wx.LB_SINGLE | wx.LB_NEEDED_SB | wx.LB_SORT)
		self.AddBtn = wx.Button(self, label = "Add")
		self.RemoveBtn = wx.Button(self, label = "Remove")
	
	def onResize(self, evt):
		(xSize, ySize) = evt.GetSize()
		
		xStep = xSize/20
		yStep = ySize/18
		
		self.Caption.SetPosition((xStep * 5, yStep))
		self.Caption.SetSize((xStep * 15, yStep * 3))
		self.List.SetPosition((xStep * 5, yStep * 4))
		self.List.SetSize((xStep * 15, yStep * 11))
		self.AddBtn.SetPosition((xStep * 5, yStep * 16))
		self.AddBtn.SetSize((xStep * 7, yStep))
		self.RemoveBtn.SetPosition((xStep * 13, yStep * 16))
		self.RemoveBtn.SetSize((xStep * 7, yStep))
