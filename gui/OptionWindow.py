import wx
import json
import os.path
import util.RessourcePath
from util.ListPanel import ListPanel
from dialogs import AddReferenceDialog, AddUserDialog, AddOfflineImageDialog, AddOnlineImageDialog, AddGlobalStateDialog

class OptionPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.HeadSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.HeadSizer.AddStretchSpacer(1)
		self.BackBtn = wx.Button(self, label="<<\nBack\n<<", style=wx.CENTER)
		self.BackBtn.Bind(wx.EVT_BUTTON, self.onBack)
		self.BackBtn.SetFont(wx.Font(18, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.HeadSizer.Add(self.BackBtn, 1, wx.EXPAND | wx.CENTER)
		
		
		self.CaptionSizer = wx.BoxSizer(wx.VERTICAL)
		self.CaptionSizer.AddStretchSpacer(1)
		self.Caption = wx.StaticText( self, label="Global Options", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.Caption.SetFont(wx.Font(34, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.CaptionSizer.Add(self.Caption, 2, wx.EXPAND)
		
		self.ListSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.ListCaptionFont = wx.Font(14, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.ListSizer.AddStretchSpacer(1)
		self.LookupPanel = ListPanel(self)
		self.LookupPanel.Caption.SetLabel("Reference Lookup")
		self.LookupPanel.Caption.SetFont(self.ListCaptionFont)
		self.LookupPanel.AddBtn.Bind(wx.EVT_BUTTON, self.onAddReference)
		self.LookupPanel.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveReference)
		self.ListSizer.Add(self.LookupPanel, 2, wx.EXPAND | wx.CENTER)
		
		self.ListSizer.AddStretchSpacer(1)
		self.UserPanel = ListPanel(self)
		self.UserPanel.Caption.SetLabel("User IDs")
		self.UserPanel.Caption.SetFont(self.ListCaptionFont)
		self.UserPanel.AddBtn.Bind(wx.EVT_BUTTON, self.onAddUser)
		self.UserPanel.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveUser)
		self.ListSizer.Add(self.UserPanel, 2, wx.EXPAND | wx.CENTER)
		
		self.ListSizer.AddStretchSpacer(1)
		self.OfflineImageList = ListPanel(self)
		self.OfflineImageList.Caption.SetLabel("Offline Images")
		self.OfflineImageList.Caption.SetFont(self.ListCaptionFont)
		self.OfflineImageList.AddBtn.Bind(wx.EVT_BUTTON, self.onAddOfflineImage)
		self.OfflineImageList.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveOfflineImage)
		self.ListSizer.Add(self.OfflineImageList, 2, wx.EXPAND | wx.CENTER)
		
		self.ListSizer.AddStretchSpacer(1)
		self.OnlineImageList = ListPanel(self)
		self.OnlineImageList.Caption.SetLabel("Image Links")
		self.OnlineImageList.Caption.SetFont(self.ListCaptionFont)
		self.OnlineImageList.AddBtn.Bind(wx.EVT_BUTTON, self.onAddOnlineImage)
		self.OnlineImageList.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveOnlineImage)
		self.ListSizer.Add(self.OnlineImageList, 2, wx.EXPAND | wx.CENTER)
		
		self.ListSizer.AddStretchSpacer(1)
		self.GlobalsList = ListPanel(self)
		self.GlobalsList.Caption.SetLabel("Global States")
		self.GlobalsList.Caption.SetFont(self.ListCaptionFont)
		self.GlobalsList.AddBtn.Bind(wx.EVT_BUTTON, self.onAddGlobalState)
		self.GlobalsList.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveGlobalState)
		self.ListSizer.Add(self.GlobalsList, 2, wx.EXPAND | wx.CENTER)
		
		self.CaptionSizer.Add(self.ListSizer, 14, wx.EXPAND)
		self.HeadSizer.Add(self.CaptionSizer, 20, wx.EXPAND)
		self.HeadSizer.AddStretchSpacer(1)
		self.SetSizer(self.HeadSizer)
		
		
		with open(util.RessourcePath.RessourcePath("data/bots.json"), 'r') as f:
			self.Data = json.load(f)
		if not os.path.exists(self.Data["lookup"]):
			self.Lookup = {"lookup" : {}, "offline" : {}, "online" : {}, "users" : {}}
		else:
			with open(util.RessourcePath.RessourcePath(self.Data["lookup"]), 'r') as f:
				self.Lookup = json.load(f)
		
		for key, value in self.Lookup["lookup"].items():
			self.LookupPanel.List.Append(key)
		for key, value in self.Lookup["users"].items():
			self.UserPanel.List.Append(key)
		for key, value in self.Lookup["offline"].items():
			self.OfflineImageList.List.Append(key)
		for key, value in self.Lookup["online"].items():
			self.OnlineImageList.List.Append(key)
		if isinstance(self.Data["globals"], str):
			if self.Data["globals"] != "":
				self.GlobalsList.List.Append(self.Data["globals"])
		else:
			for key, value in self.Data["globals"].items():
				self.GlobalsList.List.Append(key)
	
	
	def onBack(self, evt):
		self.GetParent().SetSetupPanel()
	
	def onAddReference(self, evt):
		if self.LookupPanel.List.GetSelection() != wx.NOT_FOUND:
			referenceName = self.LookupPanel.List.GetString(self.LookupPanel.List.GetSelection())
			refDialog = AddReferenceDialog.AddReferenceDialog(self, {"name" : referenceName, "value" : self.Lookup["lookup"][referenceName]})
			self.LookupPanel.List.Delete(self.LookupPanel.List.GetSelection())
		else:
			refDialog = AddReferenceDialog.AddReferenceDialog(self)
		
		if refDialog.ShowModal() == wx.ID_OK:
			self.Lookup["lookup"][refDialog.NameText.GetValue()] = refDialog.RefText.GetValue()
			self.LookupPanel.List.Append(refDialog.NameText.GetValue())
	def onRemoveReference(self, evt):
		reference = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		if reference == wx.NOT_FOUND:
			return
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Reference " + reference, "Removing " + reference, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			del self.Lookup["lookup"][reference]
	
	def onAddUser(self, evt):
		if self.UserPanel.List.GetSelection() != wx.NOT_FOUND:
			userName = self.UserPanel.List.GetString(self.UserPanel.List.GetSelection())
			refDialog = AddUserDialog.AddUserDialog(self, {"name" : userName, "value" : self.Lookup["users"][userName]})
			self.UserPanel.List.Delete(self.UserPanel.List.GetSelection())
		else:
			refDialog = AddUserDialog.AddUserDialog(self)
		
		if refDialog.ShowModal() == wx.ID_OK:
			self.Lookup["users"][refDialog.NameText.GetValue()] = refDialog.RefText.GetValue()
			self.UserPanel.List.Append(refDialog.NameText.GetValue())
	def onRemoveUser(self, evt):
		user = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		if user == wx.NOT_FOUND:
			return
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the User " + user, "User " + user, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			del self.Lookup["users"][user]
			#remove all references to user as well?
	
	def onAddOfflineImage(self, evt):
		if self.OfflineImageList.List.GetSelection() != wx.NOT_FOUND:
			userName = self.OfflineImageList.List.GetString(self.OfflineImageList.List.GetSelection())
			refDialog = AddOfflineImageDialog.AddOfflineImageDialog(self, {"name" : userName, "value" : self.Lookup["offline"][userName]})
			self.OfflineImageList.List.Delete(self.OfflineImageList.List.GetSelection())
		else:
			refDialog = AddOfflineImageDialog.AddOfflineImageDialog(self)
		
		if refDialog.ShowModal() == wx.ID_OK:
			self.Lookup["offline"][refDialog.NameText.GetValue()] = refDialog.RefText.GetValue()
			self.OfflineImageList.List.Append(refDialog.NameText.GetValue())
	def onRemoveOfflineImage(self, evt):
		offlineImage = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		if offlineImage == wx.NOT_FOUND:
			return
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the registered offline Image " + offlineImage, "Removing " + offlineImage, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			del self.Lookup["offline"][offlineImage]
	
	def onAddOnlineImage(self, evt):
		if self.OnlineImageList.List.GetSelection() != wx.NOT_FOUND:
			userName = self.OnlineImageList.List.GetString(self.OnlineImageList.List.GetSelection())
			refDialog = AddOnlineImageDialog.AddOnlineImageDialog(self, {"name" : userName, "value" : self.Lookup["online"][userName]})
			self.OnlineImageList.List.Delete(self.OnlineImageList.List.GetSelection())
		else:
			refDialog = AddOnlineImageDialog.AddOnlineImageDialog(self)
		
		if refDialog.ShowModal() == wx.ID_OK:
			self.Lookup["online"][refDialog.NameText.GetValue()] = refDialog.RefText.GetValue()
			self.OnlineImageList.List.Append(refDialog.NameText.GetValue())
	def onRemoveOnlineImage(self, evt):
		onlineImage = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		if onlineImage == wx.NOT_FOUND:
			return
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Image Link " + onlineImage, "Removing " + onlineImage, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			del self.Lookup["online"][onlineImage]
	
	def onAddGlobalState(self, evt):
		if self.GlobalsList.List.GetSelection() != wx.NOT_FOUND:
			userName = self.GlobalsList.List.GetString(self.GlobalsList.List.GetSelection())
			refDialog = AddGlobalStateDialog.AddGlobalStateDialog(self, userName)
			print("using Global Dialog")
			self.GlobalsList.List.Delete(self.GlobalsList.List.GetSelection())
		else:
			refDialog = AddGlobalStateDialog.AddGlobalStateDialog(self)
		
		if refDialog.ShowModal() == wx.ID_OK:
			if isinstance(self.Data["globals"], str):
				if self.Data["globals"] != "":
					self.Data["globals"] = [refDialog.NameText.GetValue(), self.Data["globals"]]
				else:
					self.Data["globals"] = refDialog.NameText.GetValue()
			else:
				self.Data["globals"].append(refDialog.NameText.GetValue())
			self.GlobalsList.List.Append(refDialog.NameText.GetValue())
	def onRemoveGlobalState(self, evt):
		globalState = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		if globalState == wx.NOT_FOUND:
			return
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Global " + globalState, "Removing " + globalState, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			if len(self.Data["globals"]) > 2:
				self.Data["globals"] = [entry for entry in self.Data["globals"] if entry != globalState]
			elif len(self.Data["globals"]) == 2:
				if self.Data["globals"][0] == globalState:
					self.Data["globals"] = self.Data["globals"][1]
				else:
					self.Data["globals"] = self.Data["globals"][0]
			else:
				self.Data["globals"] = ""
	
	def onClose(self):
		with open(util.RessourcePath.RessourcePath(self.Data["lookup"]), 'w') as f:
			json.dump(self.Lookup, f)
		with open(util.RessourcePath.RessourcePath("data/bots.json"), 'w') as f:
			json.dump(self.Data, f)