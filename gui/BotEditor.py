import wx
import os.path
import json

from dialogs import FriendSelector, ReactionDialog
from util.ListPanel import ListPanel
import util.RessourcePath
from EditorSections import TextEditor, StateEditor, EmbedEditor, HelpPanel

class BotEditor(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.HeadSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.LeftSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.Caption = wx.StaticText(self, label="Bot Editor", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.Caption.SetFont(self.CaptionFont)
		self.LeftSizer.Add(self.Caption, 1, wx.EXPAND)
		
		self.ListSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.FriendPanel = ListPanel(self)
		self.ListFont = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.FriendPanel.Caption.SetLabel("Friend\nList")
		self.FriendPanel.Caption.SetFont(self.ListFont)
		self.FriendPanel.List.Bind(wx.EVT_LISTBOX, self.onFriendSelect)
		self.FriendPanel.AddBtn.Bind(wx.EVT_BUTTON, self.onAddFriend)
		self.FriendPanel.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveFriend)
		self.FriendPanel.RemoveBtn.Disable()
		
		self.ListSizer.Add(self.FriendPanel, 1, wx.EXPAND)
		
		self.ReactionPanel = ListPanel(self)
		self.ReactionPanel.Caption.SetLabel("Reaction\nList")
		self.ReactionPanel.Caption.SetFont(self.ListFont)
		self.ReactionPanel.List.Bind(wx.EVT_LISTBOX, self.onReactionSelect)
		self.ReactionPanel.AddBtn.Bind(wx.EVT_BUTTON, self.onAddReaction)
		self.ReactionPanel.AddBtn.Disable()
		self.ReactionPanel.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveReaction)
		self.ReactionPanel.RemoveBtn.Disable()
		self.ListSizer.Add(self.ReactionPanel, 1, wx.EXPAND)
		self.LeftSizer.Add(self.ListSizer, 8, wx.EXPAND)
		
		self.BackBtn = wx.Button(self, label="<< Back")
		self.BtnFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.BackBtn.SetFont(self.BtnFont)
		self.BackBtn.Bind(wx.EVT_BUTTON, self.onBack)
		self.LeftSizer.Add(self.BackBtn, 1, wx.EXPAND)
		
		self.HeadSizer.Add(self.LeftSizer, 3, wx.EXPAND)
		
		
		self.BtnSizer = wx.BoxSizer(wx.VERTICAL)
		self.TextEditorButton = wx.Button(self, label="Edit Reaction Text")
		self.TextEditorButton.Disable()
		self.TextEditorButton.Bind(wx.EVT_BUTTON, self.onTextEditor)
		self.BtnSizer.AddStretchSpacer(1)
		self.BtnSizer.Add(self.TextEditorButton, 2, wx.EXPAND)
		
		self.StateEditorButton = wx.Button(self, label="Edit Reaction State")
		self.StateEditorButton.Disable()
		self.StateEditorButton.Bind(wx.EVT_BUTTON, self.onStateEditor)
		self.BtnSizer.AddStretchSpacer(1)
		self.BtnSizer.Add(self.StateEditorButton, 2, wx.EXPAND)
		
		self.EmbedEditorButton = wx.Button(self, label="Edit Reaction Embed")
		self.EmbedEditorButton.Disable()
		self.EmbedEditorButton.Bind(wx.EVT_BUTTON, self.onEmbedEditor)
		self.BtnSizer.AddStretchSpacer(1)
		self.BtnSizer.Add(self.EmbedEditorButton, 2, wx.EXPAND)
		
		self.EditorHelpButton = wx.Button(self, label="Help")
		self.EditorHelpButton.Disable()
		self.EditorHelpButton.Bind(wx.EVT_BUTTON, self.onEditorHelp)
		self.BtnSizer.AddStretchSpacer(1)
		self.BtnSizer.Add(self.EditorHelpButton, 2, wx.EXPAND)
		self.BtnSizer.AddStretchSpacer(1)
		
		self.HeadSizer.AddStretchSpacer(1)
		self.HeadSizer.Add(self.BtnSizer, 3, wx.EXPAND)
		self.HeadSizer.AddStretchSpacer(1)
		
		
		self.ReactionTextEditorPanel = TextEditor.ReactionTextEditor(self)
		self.ReactionTextEditorPanel.Hide()
		self.HeadSizer.Add(self.ReactionTextEditorPanel, 3, wx.EXPAND)
		
		self.ReactionStateEditorPanel = StateEditor.ReactionStateEditor(self)
		self.ReactionStateEditorPanel.Hide()
		self.HeadSizer.Add(self.ReactionStateEditorPanel, 3, wx.EXPAND)
		
		self.ReactionEmbedEditorPanel = EmbedEditor.ReactionEmbedEditor(self)
		self.ReactionEmbedEditorPanel.Hide()
		self.HeadSizer.Add(self.ReactionEmbedEditorPanel, 3, wx.EXPAND)
		
		self.EditorHelpPanel = HelpPanel.EditorHelpPanel(self)
		self.HeadSizer.Add(self.EditorHelpPanel, 3, wx.EXPAND)
		
		
		self.HeadSizer.AddStretchSpacer(1)
		self.SetSizer(self.HeadSizer)
		
		
		self.Reactions = {"friends":"", "system":{}, "any":{}}
		self.Lookup = {"lookup":{}, "online":{}, "offline":{}, "users":{}}
		self.BotFile = "data/dump"
		self.LookupFile = "data/dump"
	
	def onAddReaction(self, evt):
		advEditor = ReactionDialog.AddReactionDialog(self)
		rtn = advEditor.ShowModal()
		if rtn == wx.ID_OK:
			name = advEditor.Text.GetValue()
			self.ReactionPanel.List.Append(name)
			self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())][name] = {"trigger":"", "reaction":""}

	def onRemoveReaction(self, evt):
		offlineImage = self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Reaction " + offlineImage, "Removing " + offlineImage, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.ReactionPanel.List.Delete(self.ReactionPanel.List.GetSelection())
			del self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())][offlineImage]
			self.ReactionPanel.AddBtn.Disable()
			self.ReactionPanel.RemoveBtn.Disable()
			self.TextEditorButton.Disable()
			self.StateEditorButton.Disable()
			self.EmbedEditorButton.Disable()
			self.EditorHelpButton.Disable()
			self.onEditorHelp(None)
	def onAddFriend(self, evt):
		advEditor = FriendSelector.AddFriendDialog(self, list(set(list(self.Lookup["users"].keys()) + self.GetParent().GetBotList()) - set(self.FriendPanel.List.GetItems())))
		if wx.ID_OK == advEditor.ShowModal():
			name = advEditor.List.GetString(advEditor.List.GetSelection())
			if self.FriendPanel.List.FindString(name) == wx.NOT_FOUND:
				self.FriendPanel.List.Append(name)
				if isinstance(self.Reactions["friends"], str):
					self.Reactions["friends"] = [self.Reactions["friends"], name]
				else:
					self.Reactions["friends"] = self.Reactions["friends"] + [name]
			else:
				self.FriendPanel.List.SetSelection(self.FriendPanel.List.FindString(name))
			
	def onRemoveFriend(self, evt):
		offlineImage = self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())
		if not offlineImage in ["system", "any"]:
			messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Friend " + offlineImage, "Removing " + offlineImage, style=wx.YES_NO|wx.CENTRE)
			res = messageDlg.ShowModal()
			if res == wx.ID_YES:
				if isinstance(self.Reactions["friends"], str):
					if self.Reactions["friends"] == self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()):
						self.Reactions["friends"] = ""
				elif len(self.Reactions["friends"]) == 2:
					if self.Reactions["friends"][0] == self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()):
						self.Reactions["friends"] = self.Reactions["friends"][1]
					else:
						self.Reactions["friends"] = self.Reactions["friends"][0]
				else:
					self.Reactions["friends"].remove(self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()))
				self.FriendPanel.List.Delete(self.FriendPanel.List.GetSelection())
				self.ReactionPanel.AddBtn.Disable()
				self.ReactionPanel.RemoveBtn.Disable()
				self.FriendPanel.AddBtn.Disable()
				self.FriendPanel.RemoveBtn.Disable()
				self.TextEditorButton.Disable()
				self.StateEditorButton.Disable()
				self.EmbedEditorButton.Disable()
				self.EditorHelpButton.Disable()
				self.onEditorHelp(None)
	
	def onBack(self, evt):
		self.GetParent().SetSetupPanel()
	
	def onFriendSelect(self, evt):
		if not self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()) in ["system", "any"]:
			self.FriendPanel.RemoveBtn.Enable()
		else:
			self.FriendPanel.RemoveBtn.Disable()
		self.ReactionPanel.AddBtn.Enable()
		self.TextEditorButton.Disable()
		self.StateEditorButton.Disable()
		self.EmbedEditorButton.Disable()
		self.EditorHelpButton.Disable()
		self.onEditorHelp(None)
		if self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()) in self.Reactions.keys():
			self.ReactionPanel.List.Clear()
			self.ReactionPanel.List.AppendItems(list(self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())].keys()))
		else:
			self.ReactionPanel.List.Clear()
			self.Reactions.update({self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection()) : {}})
	
	def onReactionSelect(self, evt):
		self.ReactionPanel.RemoveBtn.Enable()
		
		self.TextEditorButton.Enable()
		self.StateEditorButton.Enable()
		self.EmbedEditorButton.Enable()
		self.EditorHelpButton.Enable()
	
	def setFile(self, file, Lookup):
		self.BotFile = util.RessourcePath.RessourcePath(file)
		self.LookupFile = util.RessourcePath.RessourcePath(Lookup)
		if os.path.isfile(file):
			with open(self.BotFile) as botFile:
				self.Reactions = json.load(botFile)
		else:
			self.Reactions = {"friends":"", "system":{}, "any":{}}
			
		if os.path.isfile(Lookup):
			with open(util.RessourcePath.RessourcePath(Lookup)) as LookupFile:
				self.Lookup = json.load(LookupFile)
		else:
			self.Lookup = {"lookup":{}, "online":{}, "offline":{}, "users":{}}
		
		if isinstance(self.Reactions["friends"], str):
			if not self.Reactions["friends"] == "":
				self.FriendPanel.List.Clear()
				self.FriendPanel.List.AppendItems(list(set([self.Reactions["friends"], "system", "any"])))
			else:
				self.FriendPanel.List.Clear()
				self.FriendPanel.List.AppendItems(["system", "any"])
		else:
			self.FriendPanel.List.Clear()
			self.FriendPanel.List.AppendItems(list(set(self.Reactions["friends"] + ["system", "any"])))

	def onTextEditor(self, evt):
		reaction = self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())][self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())]
		self.ReactionTextEditorPanel.TriggerTextCtrl.SetValue(reaction["trigger"])
		self.ReactionTextEditorPanel.ReactionTextCtrl.SetValue(reaction["reaction"])
		
		if not self.ReactionTextEditorPanel.IsShown():
			self.ReactionTextEditorPanel.Show()
			self.ReactionStateEditorPanel.Hide()
			self.ReactionEmbedEditorPanel.Hide()
			self.EditorHelpPanel.Hide()
			self.Layout()
	
	def onStateEditor(self, evt):
		reaction = self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())][self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())]
		self.ReactionStateEditorPanel.StateList.DeleteAllItems()
		if "state" in reaction:
			for key, value in reaction["state"].items():
				self.ReactionStateEditorPanel.StateList.Append((key, value))
		if not self.ReactionStateEditorPanel.IsShown():
			self.ReactionTextEditorPanel.Hide()
			self.ReactionStateEditorPanel.Show()
			self.ReactionEmbedEditorPanel.Hide()
			self.EditorHelpPanel.Hide()
			self.Layout()
	
	def onEmbedEditor(self, evt):
		reaction = self.Reactions[self.FriendPanel.List.GetString(self.FriendPanel.List.GetSelection())][self.ReactionPanel.List.GetString(self.ReactionPanel.List.GetSelection())]
		if "image" in reaction:
			if "embed" in reaction:
				self.ReactionEmbedEditorPanel.EmbedEntry.SetValue(reaction["image"]["embed"])
		if not self.ReactionEmbedEditorPanel.IsShown():
			self.ReactionTextEditorPanel.Hide()
			self.ReactionStateEditorPanel.Hide()
			self.ReactionEmbedEditorPanel.Show()
			self.EditorHelpPanel.Hide()
			self.Layout()
		
	def onEditorHelp(self, evt):
		if not self.EditorHelpPanel.IsShown():
			self.ReactionTextEditorPanel.Hide()
			self.ReactionStateEditorPanel.Hide()
			self.ReactionEmbedEditorPanel.Hide()
			self.EditorHelpPanel.Show()
			self.Layout()
		
	
	def onClose(self):
		with open(self.BotFile, 'w') as f:
			json.dump(self.Reactions, f)
		with open(self.LookupFile, 'w') as f:
			json.dump(self.Lookup, f)
