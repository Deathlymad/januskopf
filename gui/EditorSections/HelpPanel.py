import wx
import wx.html

class LinkWrapper(wx.html.HtmlWindow):
	def OnLinkClicked(self, link):
		webbrowser.open(link.GetHref());

class EditorHelpPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.TextSizer = wx.BoxSizer(wx.VERTICAL)
		self.Hints = LinkWrapper(self)
		self.Hints.SetPage("""
		<ul>
		<li>First, enter a Bot Token:
		<ol type="1">
			<li>Go to the <a href=https://discordapp.com/developers/applications/me>Discord Dev Website</a> and add a new App.</li>
			<li>Define the Application as a Bot Account while clicking on "Create Bot User".</li>
			<li>Now you can reveal a Token on the Bot Panel. Copy it in the Text Box labeled "Discord Bot Token".</li>
		</ol></li>
		<li>The Name does not have to be the exact Name of the Bot. Any Alias works, as it is mainly used internally.</li>
		<li>Since the Command Prefix for the Bot is based on the Name, it is however recommended.</li>
		<li>The TalkFile is a JSON file that contains the Information of all Interactions the Bot is capable of doing. Select a Location to store it.</li>
		<li>The Image Search Path is for looking up Images stored offline that can be uploaded in conjunction with reactions.</li>
		</ul>
		""")
		self.TextSizer.Add(self.Hints, 1, wx.EXPAND)
		
		self.SetSizer(self.TextSizer)