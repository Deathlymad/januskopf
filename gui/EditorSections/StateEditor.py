import wx

class ReactionStateEditor(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		
		self.EditorSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.StateList = wx.ListCtrl(self, style=wx.LC_REPORT)
		self.StateList.AppendColumn("State")
		self.StateList.AppendColumn("Value")
		self.EditorSizer.Add(self.StateList, 5, wx.EXPAND)
		self.StateButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.AddStateBtn = wx.Button(self, label = "Add")
		self.AddStateBtn.Bind(wx.EVT_BUTTON, self.onAddState)
		self.StateButtonSizer.Add(self.AddStateBtn, 5, wx.EXPAND)
		self.StateButtonSizer.AddStretchSpacer(1)
		self.RemoveStateBtn = wx.Button(self, label = "Remove")
		self.RemoveStateBtn.Bind(wx.EVT_BUTTON, self.onRemoveState)
		self.StateButtonSizer.Add(self.RemoveStateBtn, 5, wx.EXPAND)
		self.EditorSizer.AddStretchSpacer(1)
		self.EditorSizer.Add(self.StateButtonSizer, 1, wx.EXPAND)
		
		self.SetSizer(self.EditorSizer)
	
	def onAddState(self, evt):
		select = self.StateList.GetFocusedItem()
		advEditor = None
		if select != wx.NOT_FOUND:
			advEditor = StateMutator.AddStateDialog(self, {"name":self.StateList.GetItem(select).GetText(),"state":self.StateList.GetItem(select, 1).GetText()})
			self.StateList.DeleteItem(select)
		else:
			advEditor = StateMutator.AddStateDialog(self)
		if advEditor.ShowModal() == wx.ID_OK:
			self.StateList.Append((advEditor.NameText.GetValue(), advEditor.StateText.GetValue()))
			if not "state" in self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]:
				self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]["state"] = {}
			self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]["state"][advEditor.NameText.GetValue()] = advEditor.StateText.GetValue()
	def onRemoveState(self, evt):
		select = self.StateList.GetFocusedItem()
		if select != wx.NOT_FOUND:
			state = self.StateList.GetItem(select).GetText()
			messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the State " + state, "Removing " + state, style=wx.YES_NO|wx.CENTRE)
			res = messageDlg.ShowModal()
			if res == wx.ID_YES:
				self.StateList.DeleteItem(select)
				del self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]["state"][state]
	