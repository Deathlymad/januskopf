import wx
from util.ListPanel import ListPanel
from dialogs import OfflineImageSelector, AddOfflineImageDialog, AddOnlineImageDialog, EmbedDialog

class ReactionEmbedEditor(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		
		self.EditorSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.SelectEmbed = wx.Button(self, label="Select Embed", style=wx.ST_NO_AUTORESIZE)
		self.SelectEmbed.Bind(wx.EVT_BUTTON, self.selectEmbed)
		self.EditorSizer.AddStretchSpacer(1)
		self.EditorSizer.Add(self.SelectEmbed, 3, wx.EXPAND)
		self.EditorSizer.AddStretchSpacer(1)
		
		self.OfflineImageList = ListPanel(self)
		self.OfflineImageList.Caption.SetLabel("Offline Images")
		self.OfflineImageList.Caption.SetFont(wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.OfflineImageList.AddBtn.Bind(wx.EVT_BUTTON, self.onAddOfflineImage)
		self.OfflineImageList.RemoveBtn.Bind(wx.EVT_BUTTON, self.onRemoveOfflineImage)
		self.OfflineImageList.RemoveBtn.Disable()
		self.EditorSizer.Add(self.OfflineImageList, 3, wx.EXPAND)
		self.EditorSizer.AddStretchSpacer(1)
		
		self.SetSizer(self.EditorSizer)
		
	
	def onAddOfflineImage(self, evt):
		advEditor = SelectOfflineImagesDialog(self)
		if advEditor.ShowModal() == wx.ID_OK:
			self.OfflineImageList.Append(advEditor.List.GetString(advEditor.List.GetSelection()))
	def onRemoveOfflineImage(self, evt):
		offlineImage = self.OfflineImageList.GetString(self.OfflineImageList.GetSelection())
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Image " + offlineImage, "Removing " + offlineImage, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			self.OfflineImageList.Delete(self.OfflineImageList.GetSelection())
	
	def selectEmbed(self, evt):
		lookups = self.GetParent().LookupFile
		if "online" in lookups:
			advEditor = EmbedDialog.SelectEmbedDialog(self, list(lookups["online"].keys()))
		else:
			advEditor = EmbedDialog.SelectEmbedDialog(self)
		if advEditor.ShowModal() == wx.ID_OK:
			self.EmbedEntry.SetValue(advEditor.List.GetString(advEditor.List.GetSelection))
	