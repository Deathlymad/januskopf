import wx

from util.HighlightTextCtrl import HighlightTextCtrl

class ReactionTextEditor(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.CaptionFont = wx.Font(24, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		
		self.EditorSizer = wx.BoxSizer(wx.VERTICAL)
		self.TriggerCaption = wx.StaticText(self, label="Reaction Trigger:", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.TriggerCaption.SetFont(self.CaptionFont)
		self.EditorSizer.Add(self.TriggerCaption, 1, wx.EXPAND | wx.ALIGN_LEFT)
		self.TriggerAdvBtn = wx.Button(self, label="Advanced Trigger\nEditor", style=wx.ST_NO_AUTORESIZE)
		self.TriggerAdvBtn.Bind(wx.EVT_BUTTON, self.openAdvancedTriggerEditor)
		self.EditorSizer.Add(self.TriggerAdvBtn, 1, wx.EXPAND | wx.ALIGN_RIGHT)
		self.TriggerTextCtrl = HighlightTextCtrl(self, "")
		self.TriggerTextCtrl.Bind(wx.EVT_TEXT, self.onEditTrigger)
		self.EditorSizer.Add(self.TriggerTextCtrl, 6, wx.EXPAND)
		self.EditorSizer.AddStretchSpacer(1)
		
		self.ReactionCaption = wx.StaticText(self, label="Reaction:", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.ReactionCaption.SetFont(self.CaptionFont)
		self.EditorSizer.Add(self.ReactionCaption, 1, wx.EXPAND | wx.ALIGN_LEFT)
		self.ReactionAdvBtn = wx.Button(self, label="Advanced Reaction\nEditor", style=wx.ST_NO_AUTORESIZE)
		self.ReactionAdvBtn.Bind(wx.EVT_BUTTON, self.openAdvancedReactionEditor)
		self.EditorSizer.Add(self.ReactionAdvBtn, 1, wx.EXPAND | wx.ALIGN_RIGHT)
		self.ReactionTextCtrl = HighlightTextCtrl(self, "")
		self.ReactionTextCtrl.Bind(wx.EVT_TEXT, self.onEditReaction)
		self.EditorSizer.Add(self.ReactionTextCtrl, 8, wx.EXPAND)
		self.EditorSizer.AddStretchSpacer(1)
		
		self.SetSizer(self.EditorSizer)
	
	def onEditTrigger(self, evt):
		self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]["trigger"] = self.TriggerTextCtrl.GetValue()
	def onEditReaction(self, evt):
		self.GetParent().Reactions[self.GetParent().FriendPanel.List.GetString(self.GetParent().FriendPanel.List.GetSelection())][self.GetParent().ReactionPanel.List.GetString(self.GetParent().ReactionPanel.List.GetSelection())]["reaction"] = self.ReactionTextCtrl.GetValue()
	
	def openAdvancedTriggerEditor(self, evt):
		advEditor = AdvancedEditor.AdvancedEditor(self, self.TriggerTextCtrl.GetValue(), self.GetParent().Lookup["lookup"])
		advEditor.ShowModal()
		self.TriggerTextCtrl.SetValue(advEditor.Editor.GetValue())
		self.onEditTrigger(None)
	def openAdvancedReactionEditor(self, evt):
		advEditor = AdvancedEditor.AdvancedEditor(self, self.ReactionTextCtrl.GetValue(), self.GetParent().Lookup["lookup"])
		advEditor.ShowModal()
		self.ReactionTextCtrl.SetValue(advEditor.Editor.GetValue())
		self.onEditReaction(None)