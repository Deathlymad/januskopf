import wx
import json
import os
import util.RessourcePath
from util.Bot import ManagedBot
import time

class RuntimeInterface(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.HeadSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.HeadSizer.AddStretchSpacer(1)
		
		
		self.BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.BtnSizer.AddStretchSpacer(1)
		self.BackBtn = wx.Button(self, label="<<\nBack\n<<")
		self.BackBtn.SetFont(wx.Font(30, wx.DECORATIVE, wx.NORMAL, wx.BOLD))
		self.BackBtn.Bind(wx.EVT_BUTTON, self.onBack)
		self.BtnSizer.Add(self.BackBtn, 2, wx.EXPAND)
		self.BtnSizer.AddStretchSpacer(1)
		
		self.Log = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY)
		self.BtnSizer.Add(self.Log, 20, wx.EXPAND)
		self.BtnSizer.AddStretchSpacer(1)
		
		self.HeadSizer.Add(self.BtnSizer, 24, wx.EXPAND)
		
		self.HeadSizer.AddStretchSpacer(1)
		
		self.SetSizer(self.HeadSizer)
		self.running = False
		
		self.bots = []
		
	
	def onBack(self, evt):
		self.onClose()
		self.GetParent().SetSetupPanel()
	
	def Run(self):
		if self.running:
			return
		self.Layout()
		with open(util.RessourcePath.RessourcePath("data/bots.json"), 'r') as f:
			data = json.load(f)

		self.Log.write("Loading Message Lookup Table.\n")
		with open(util.RessourcePath.RessourcePath(data["lookup"]), 'r') as f:
			lookup = json.load(f)

		if os.path.isfile("data/globalStates.state"):
			self.Log.write("Loading last global state\n")
			with open(util.RessourcePath.RessourcePath("data/globalStates.state"), 'r') as f:
				ManagedBot.globalStates = json.load(f)

		GLOBAL_LIST = []
		if isinstance(data["globals"], str):
			if data["globals"] != "":
				GLOBAL_LIST = [data["globals"]]
		elif isinstance(data["globals"], list):
			GLOBAL_LIST = data["globals"]

		self.Log.write("Ensuring all Globals are initialized\n")
		for entry in GLOBAL_LIST:
			if not entry in ManagedBot.globalsDict:
				ManagedBot.globalsDict[entry] = ""

		self.Log.write("Deleting obsolete Globals.\n")
		for entry in [entry for entry, value in ManagedBot.globalsDict if not entry in GLOBAL_LIST]:
			del ManagedBot.globalsDict[entry]


		#Initialization
		self.Log.write("Initializing Bots.\n")
		for botName, botData in data["bots"].items():
			self.bots.append(
				ManagedBot(
					self.Log,
					botData["token"],
					botName,
					botData["talkFile"],
					botData["localImagePath"]))

		self.Log.write("Loading Bot Data from File.\n")
		for bot in self.bots:
			bot.load(lookup)

		self.Log.write("Logging in Bots.\n")
		for bot in self.bots:
			bot.run()

		while True:
			are_bots_ready = True
			for bot in self.bots:
				are_bots_ready = are_bots_ready and bot.isLoggedIn()
			if are_bots_ready:
				break

		time.sleep(10)
		self.Log.write("Crossregistering IDs.\n")
		for bot in self.bots:
			lookup.update(bot.getUserID())
		for bot in self.bots:
			bot.resolveUserIDs(lookup)

		self.Log.write("Bots are life.\n")
		
		self.running = True
	
	def onClose(self):
		if not self.running:
			return
		self.running = False
		self.Log.write("Shutting down.\n")

		for bot in self.bots:
			bot.stop()

		self.Log.write("saving global State\n")
		with open(util.RessourcePath.RessourcePath("data/globalStates.state"), 'w+') as f:
			json.dump(ManagedBot.globalsDict, f)
		
		self.bots = []
		
		self.Log.write("Shut down gracefully.\n")
		