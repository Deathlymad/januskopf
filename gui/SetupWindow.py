import wx
import json
import os.path

import BotEditor
import OptionWindow
import RuntimeInterface
import util.RessourcePath
from dialogs import CreateBotDialog

class SetupScreen(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		if (os.path.isfile(util.RessourcePath.RessourcePath("data/bots.json"))):
			with open(util.RessourcePath.RessourcePath("data/bots.json"), 'r') as f:
				self.BotData = json.load(f)
		else:
			self.BotData = {"lookup" : "data/Lookup.json",
							"globals" : "",
							"bots": {}}
		
		self.HeadSizer = wx.BoxSizer(wx.VERTICAL)
		self.HeadSizer.AddStretchSpacer(5)
		
		self.caption = wx.StaticText(self, label="JanusKopf Setup Interface", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.CaptionFont = wx.Font(42, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.BtnFont = wx.Font(18, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
		self.ListFont = wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.NORMAL)
		self.caption.SetFont(self.CaptionFont)
		self.HeadSizer.Add(self.caption, 20, wx.EXPAND | wx.ALIGN_TOP)
			
		self.CenterSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.CenterSizer.AddStretchSpacer(1)
		self.LeftBtnSizer = wx.BoxSizer(wx.VERTICAL)
		self.CreateBotBtn = wx.Button(self, label="Add Bot")
		self.CreateBotBtn.SetFont(self.BtnFont)
		self.CreateBotBtn.Bind(wx.EVT_BUTTON, self.onBotCreate)
		self.LeftBtnSizer.Add(self.CreateBotBtn, 2, wx.ALIGN_TOP | wx.EXPAND)
		self.LeftBtnSizer.AddStretchSpacer(1)
		self.EditBotBtn = wx.Button(self, label="Edit Bot")
		self.EditBotBtn.SetFont(self.BtnFont)
		self.EditBotBtn.Disable()
		self.EditBotBtn.Bind(wx.EVT_BUTTON, self.onBotEdit)
		self.LeftBtnSizer.Add(self.EditBotBtn, 3, wx.ALIGN_CENTER | wx.EXPAND)
		self.LeftBtnSizer.AddStretchSpacer(1)
		self.RemoveBotBtn = wx.Button(self, label="Remove Bot")
		self.RemoveBotBtn.SetFont(self.BtnFont)
		self.RemoveBotBtn.Bind(wx.EVT_BUTTON, self.onRemoveBot)
		self.RemoveBotBtn.Disable()
		self.LeftBtnSizer.Add(self.RemoveBotBtn, 2, wx.ALIGN_BOTTOM | wx.EXPAND)
		self.CenterSizer.Add(self.LeftBtnSizer, 8, wx.ALIGN_LEFT | wx.EXPAND)
		self.CenterSizer.AddStretchSpacer(1)
		
		self.ListSizer = wx.BoxSizer(wx.VERTICAL)
		self.ListCaption = wx.StaticText(self, label="Registered\nBots", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
		self.ListCaption.SetFont(self.ListFont)
		self.ListSizer.AddStretchSpacer(1)
		self.ListSizer.Add(self.ListCaption, 2, wx.EXPAND)
		self.ListSizer.AddStretchSpacer(1)
		self.BotList = wx.ListBox(self, choices=[botName for botName, botData in self.BotData["bots"].items()], style=wx.LB_SINGLE | wx.LB_NEEDED_SB | wx.LB_SORT)
		self.BotList.SetFont(self.ListFont)
		self.BotList.Bind(wx.EVT_LISTBOX, self.onListSelect)
		self.BotList.Bind(wx.EVT_LISTBOX_DCLICK, self.onListDblClick)
		self.BotList.Bind(wx.EVT_CHAR, self.onListKeyPressed)
		self.ListSizer.Add(self.BotList, 14, wx.EXPAND)
		self.CenterSizer.Add(self.ListSizer, 5, wx.ALIGN_CENTER | wx.EXPAND)
		self.CenterSizer.AddStretchSpacer(1)
		
		self.RightBtnSizer = wx.BoxSizer(wx.VERTICAL)
		self.OptionsBtn = wx.Button(self, label="Options")
		self.OptionsBtn.Bind(wx.EVT_BUTTON, self.onOptions)
		self.OptionsBtn.SetFont(self.BtnFont)
		self.RightBtnSizer.Add(self.OptionsBtn, 2, wx.ALIGN_TOP | wx.EXPAND)
		self.RightBtnSizer.AddStretchSpacer(1)
		self.RunBtn = wx.Button(self, label="Run")
		self.RunBtn.Bind(wx.EVT_BUTTON, self.onStart)
		self.RunBtn.SetFont(self.BtnFont)
		self.RightBtnSizer.Add(self.RunBtn, 3, wx.ALIGN_CENTER | wx.EXPAND)
		self.RightBtnSizer.AddStretchSpacer(1)
		self.ExitBtn = wx.Button(self, label="Exit")
		self.ExitBtn.SetFont(self.BtnFont)
		self.ExitBtn.Bind(wx.EVT_BUTTON, self.onExit)
		self.RightBtnSizer.Add(self.ExitBtn, 2, wx.ALIGN_BOTTOM | wx.EXPAND)
		self.CenterSizer.Add(self.RightBtnSizer, 8, wx.ALIGN_RIGHT | wx.EXPAND)
		self.CenterSizer.AddStretchSpacer(1)
		
		self.HeadSizer.Add(self.CenterSizer, 100, wx.EXPAND | wx.ALIGN_BOTTOM)
		self.HeadSizer.AddStretchSpacer(5)
		
		self.SetSizer(self.HeadSizer)
		
	def onListSelect(self, evt):
		self.EditBotBtn.Enable()
		self.RemoveBotBtn.Enable()
	def onListDblClick(self, evt):
		self.onBotEdit(None)
	def onListKeyPressed(self, evt):
		if evt.GetKeyCode() == wx.WXK_DELETE:
			self.onRemoveBot(None);
	
	def onBotCreate(self, evt):
		botCreator = CreateBotDialog.BotCreateDialog(self)
		res = botCreator.ShowModal()
		if (res == 0):
			self.BotData["bots"].update(botCreator.getData())
			self.BotList.Append(botCreator.getBotName())
		
		botCreator.Destroy()
		
	def onBotEdit(self, evt):
		self.GetParent().SetEditorPanel(self.BotData["bots"][self.BotList.GetString(self.BotList.GetSelection())]["talkFile"], self.BotData["lookup"])
	
	def onRemoveBot(self, evt):
		botName = self.BotList.GetString(self.BotList.GetSelection())
		messageDlg = wx.MessageDialog(self, "Are you sure you want to remove the Bot " + botName, "Removing " + botName, style=wx.YES_NO|wx.CENTRE)
		res = messageDlg.ShowModal()
		if res == wx.ID_YES:
			del self.BotData["bots"][botName]
			self.BotList.Delete(self.BotList.GetSelection())
			self.EditBotBtn.Disable()
			self.RemoveBotBtn.Disable()
	
	def onOptions(self, evt):
		self.GetParent().SetOptionPanel()
	def onStart(self, evt):
		self.GetParent().StartRuntime()
	def onExit(self, evt):
		self.GetParent().Close()
	
	def GetBotList(self):
		return list(self.BotData["bots"].keys())
		
	def onClose(self):
		with open(util.RessourcePath.RessourcePath("data/bots.json"), 'w') as f:
			json.dump(self.BotData, f)
	
#build all panels and switch
class HostWindow(wx.Frame):
	def __init__(self):
		wx.Frame.__init__(self, None, pos=(200,100), size=(800,600), style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)
		
		self.HeadSizer = wx.BoxSizer(wx.VERTICAL)
		self.SetupScreen = SetupScreen(self)
		self.HeadSizer.Add(self.SetupScreen, 1, wx.EXPAND)
		self.BotEditor = BotEditor.BotEditor(self)
		self.BotEditor.Hide()
		self.HeadSizer.Add(self.BotEditor, 1, wx.EXPAND)
		self.OptionWindow = OptionWindow.OptionPanel(self)
		self.OptionWindow.Hide()
		self.HeadSizer.Add(self.OptionWindow, 1, wx.EXPAND)
		self.RuntimeInterface = RuntimeInterface.RuntimeInterface(self)
		self.RuntimeInterface.Hide()
		self.HeadSizer.Add(self.RuntimeInterface, 1, wx.EXPAND)
		self.SetSizer(self.HeadSizer)
		
		self.Bind(wx.EVT_CLOSE, self.onClose)
	
	def GetBotList(self):
		return self.SetupScreen.GetBotList()
	
	def SetEditorPanel(self, talkFile, Lookup):
		if self.SetupScreen.IsShown():
			self.SetupScreen.Hide()
			self.SetupScreen.onClose()
		if self.OptionWindow.IsShown():
			self.OptionWindow.Hide()
			self.OptionWindow.onClose()
		if self.RuntimeInterface.IsShown():
			self.RuntimeInterface.Hide()
			self.RuntimeInterface.onClose()
		self.Layout()
		self.BotEditor.Show()
		self.BotEditor.setFile(talkFile, Lookup)
		self.SetSize((1200, 600))
		self.Layout()
	
	def SetOptionPanel(self):
		if self.SetupScreen.IsShown():
			self.SetupScreen.Hide()
			self.SetupScreen.onClose()
		if self.BotEditor.IsShown():
			self.BotEditor.Hide()
			self.BotEditor.onClose()
		if self.RuntimeInterface.IsShown():
			self.RuntimeInterface.Hide()
			self.RuntimeInterface.onClose()
		self.Layout()
		self.OptionWindow.Show()
		self.SetSize((1200, 600))
		self.Layout()
		
	def SetSetupPanel(self):
		if self.BotEditor.IsShown():
			self.BotEditor.Hide()
			self.BotEditor.onClose()
		if self.OptionWindow.IsShown():
			self.OptionWindow.Hide()
			self.OptionWindow.onClose()
		if self.RuntimeInterface.IsShown():
			self.RuntimeInterface.Hide()
			self.RuntimeInterface.onClose()
		self.Layout()
		self.SetupScreen.Show()
		self.SetSize((800, 600))
		self.Layout()
		
	def StartRuntime(self):
		if self.BotEditor.IsShown():
			self.BotEditor.Hide()
			self.BotEditor.onClose()
		if self.OptionWindow.IsShown():
			self.OptionWindow.Hide()
			self.OptionWindow.onClose()
		if self.SetupScreen.IsShown():
			self.SetupScreen.Hide()
			self.SetupScreen.onClose()
		self.Layout()
		self.RuntimeInterface.Show()
		self.SetSize((800, 600))
		self.Layout()
		self.RuntimeInterface.Run()
		
	def onClose(self, evt):
		self.SetupScreen.onClose()
		self.BotEditor.onClose()
		evt.Skip()
