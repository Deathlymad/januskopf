"""Head file for Jasonkopf Bot Manager"""

import json
import time
import os.path
import Bot

def execute():
    with open("data/bots.json", 'r') as f:
        data = json.load(f)

    print("Loading Message Lookup Table.")
    with open("data/" + data["lookup"], 'r') as f:
        lookup = json.load(f)

    if os.path.isfile("data/globalStates.state"):
        print("Loading last global state")
        with open("data/globalStates.state", 'r') as f:
            Bot.ManagedBot.globalStates = json.load(f)

    GLOBAL_LIST = []
    if isinstance(data["globals"], str):
        if data["globals"] != "":
            GLOBAL_LIST = [data["globals"]]
    elif isinstance(data["globals"], list):
        GLOBAL_LIST = data["globals"]

    print("Ensuring all globals are initialized")
    for entry in GLOBAL_LIST:
        if not entry in Bot.ManagedBot.globalsDict:
            Bot.ManagedBot.globalsDict[entry] = ""

    print("Deleting obsolete globals")
    for entry in [entry for entry, value in Bot.ManagedBot.globalsDict if not entry in GLOBAL_LIST]:
        del Bot.ManagedBot.globalsDict[entry]

    bots = []

    #Initialization
    print("Initializing Bots.")
    for botName, botData in data["bots"].items():
        bots.append(
            Bot.ManagedBot(
                botData["token"],
                botName,
                botData["talkFile"],
                botData["localImagePath"]))

    print("Loading Bot Data from File.")
    for bot in bots:
        bot.load(lookup)

    print("Logging in Bots.")
    for bot in bots:
        bot.run()

    while True:
        are_bots_ready = True
        for bot in bots:
            are_bots_ready = are_bots_ready and bot.isLoggedIn()
        if are_bots_ready:
            break

    time.sleep(10)
    print("Crossregistering IDs")
    for bot in bots:
        lookup.update(bot.getUserID())
    for bot in bots:
        bot.resolveUserIDs(lookup)

    print("Bots are life.")
    try:
        while True:
            pass
    except KeyboardInterrupt as e:
        pass

    print("Shutting down.")

    for bot in bots:
        bot.stop()

    print("saving global State")
    with open("data/globalStates.state", 'w+') as f:
        json.dump(Bot.ManagedBot.globalsDict, f)

    print("Shut down gracefully.")

if __name__ == "__main__":
	execute()
