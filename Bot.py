import json
import random
import threading
import re
import os.path
import copy
import asyncio

import discord
from discord.ext import commands
from discord.ext.commands import Bot, Command


def resolveLookup(str, table):
    lookupRegex = re.compile(r"(q{[^\}\s]+};)")

    changes = {}
    for match in lookupRegex.finditer(str):
        name = match.group(0)[2:len(match.group(0)) - 2]
        if name in table.keys():
            changes[match.group(0)] = table[name]

    for match, replace in changes.items():
        str = str.replace(match, replace)
    return str

def resolveVariables(str, localList):
    lookupRegex = re.compile(r"(v{[^\}\s]+};)")

    changes = {}
    for match in lookupRegex.finditer(str):
        name = match.group(0)[2:len(match.group(0)) - 2]
        if name in ManagedBot.globalsDict.keys():
            changes[match.group(0)] = ManagedBot.globalsDict[name]
        elif name in localList.keys():
            changes[match.group(0)] = localList[name]
        else:
            changes[match.group(0)] = "True"

    for match, replace in changes.items():
        str = str.replace(match, replace)

    return str

def updateVariables(states):
    completed = []
    for stateKey, stateValue in states.items():
        if stateKey in ManagedBot.globalsDict.keys():
            ManagedBot.globalsDict[stateKey] = stateValue
            completed.append(stateKey)
    return {stateKey: stateValue
            for stateKey, stateValue in states.items()
            if not stateKey in completed}


def evaluate(expr):
    return eval(expr.replace('&&', 'and').replace('||', 'or').replace('!', 'not '))

def parseCondition(str):
    lookupRegex = re.compile(r"c{(.+)\?(.+)\:(.+)};")

    changes = {}

    for match in lookupRegex.finditer(str):
        if evaluate(match.group(1)):
            changes[match.group(0)] = match.group(2)
        else:
            changes[match.group(0)] = match.group(3)

    for match, replace in changes.items():
        str = str.replace(match, replace)
    return str

class ManagedBot:

    globalsDict = {}

    def __init__(self, token, name, talkfile, ImagePath):
        self.loop = asyncio.new_event_loop()
        self.UserBot = Bot(command_prefix=(name.replace(" ", "") + "->"), loop=self.loop)

        self.UserBot.add_command(self.come)
        self.UserBot.add_command(self.shutup)
        self.UserBot.add_command(self.reload)

        self.UserBot.add_listener(self.on_ready)
        self.UserBot.add_listener(self.on_message)

        self.here = True
        self.name = name
        self.token = token
        self.talkfile = talkfile
        self.imagePath = ImagePath
        self.sender = {}
        self.userList = {}

    def initEntries(self):
        localTalkData = copy.deepcopy(self.talkData)
        for knownMember in localTalkData["friends"] + ["any", "system"]:
            self.sender.update({knownMember : localTalkData[knownMember]})
            for name, entry in self.sender[knownMember].items():
                entry["reaction"] = resolveLookup(entry["reaction"], self.lookup["lookup"])
                entry["trigger"] = resolveLookup(entry["trigger"], self.lookup["lookup"])
                entry["reaction"] = resolveVariables(entry["reaction"], self.localList)
                entry["trigger"] = resolveVariables(entry["trigger"], self.localList)
                entry["reaction"] = parseCondition(entry["reaction"])
                entry["trigger"] = parseCondition(entry["trigger"])
                entry["regex"] = re.compile(entry["trigger"])
                entry["name"] = name
        #self.sender = {key : value.values() for key, value in self.sender.items()}

    def load(self, lookupFile):
        self.lookup = lookupFile

        with open("data/" + self.talkfile) as f:
            self.talkData = json.load(f)

        if os.path.isfile("data/" + self.name + ".state"):
            with open("data/" + self.name + ".state", 'r') as f:
                self.localList = json.load(f)
        else:
            self.localList = {}

        self.initEntries()


    def run(self):
        self.thread = threading.Thread(target=self.run_intern)
        self.thread.start()

    def run_intern(self):
        asyncio.set_event_loop(self.loop)
        self.fut = asyncio.ensure_future(self.UserBot.start(self.token))
        try:
            self.loop.run_until_complete(self.fut)
        except Exception as e:
            pass

    def stop(self):
        print("Saving State of " + self.name)
        with open("data/" + self.name + ".state", 'w') as f:
            json.dump(self.localList, f)
        print("Killing Bot " + self.name)
        self.fut.cancel()
        print("Logging out " + self.name)
        self.fut = asyncio.ensure_future(self.UserBot.logout())
        try:
            self.loop.run_until_complete(self.fut)
        except Exception as e:
            pass
        print("Joining Thread of " + self.name)
        self.thread.join()


    def getUserID(self):
        return {str(self.UserBot.user.id) : self.UserBot.user.name}

    def resolveUserIDs(self, userList):
        self.userList = userList

    def isLoggedIn(self):
        return self.UserBot.is_logged_in

    def getRandomMessage(self, tag, sender="system"):
        if sender == "system":
            responses = [obj for obj in self.sender["system"] if obj["regex"].match(tag)]
            if len(responses) == 0:
                return {}
            elif len(responses) == 1:
                self.localList["last"] = "system." + responses[0]
                return responses[0]
            else:
                return random.choice(responses)
        else:
            responses = [obj for obj in self.sender["any"] if obj["regex"].match(tag)]
            if not sender in ["any", "system"] and sender in self.sender.keys():
                responses = responses + [obj
                                         for obj in self.sender[sender]
                                         if obj["regex"].match(tag)]

            if len(responses) == 0:
                return {}
            elif len(responses) == 1:
                return responses[0]
            else:
                return random.choice(responses)

    async def sendMessage(self, channel, message):
        if "image" in message.keys():
            e = None
            if "embed" in message["image"].keys():
                e = discord.Embed()
                if message["image"]["embed"] in self.lookup["online"]:
                    e.set_image(url=self.lookup["online"][message["image"]["embed"]])
                    e.image.width = 250
                    e.image.heigth = 250
                    e.set_footer(text=message["image"]["embed"])

            if "reaction" in message:
                await self.UserBot.send_message(channel, message["reaction"], embed=e)
            else:
                await self.UserBot.send_message(channel, embed=e)

            if "offline" in message["image"].keys():
                if isinstance(message["image"]["offline"], str):
                    pos = message["image"]["offline"].find(".")
                    isSet = False
                    if pos > 0:
                        isSet = message["image"]["offline"][:pos] == "set"
                    if isSet:
                        if message["image"]["offline"][pos:] in self.lookup["offline"]["sets"]:
                            message["image"]["offline"] = self.lookup["offline"]["sets"][message["image"]["offline"][pos:]]
                    else:
                        message["image"]["offline"] = self.lookup["offline"][message["image"]["offline"]]

                #Reevaluate since the set might have been resolved
                if isinstance(message["image"]["offline"], str):
                    if os.path.isfile(self.imagePath + message["image"]["offline"]):
                        await self.UserBot.send_file(channel, self.imagePath + message["image"]["offline"])
                elif isinstance(message["image"]["offline"], list):
                    for image in message["image"]["offline"]:
                        if os.path.isfile(self.imagePath + image):
                            await self.UserBot.send_file(channel, self.imagePath + image)
        else:
            if "reaction" in message:
                await self.UserBot.send_message(channel, message["reaction"])
        if "state" in message:
            self.localList.update(updateVariables(message["state"]))
            self.here = False
            self.initEntries()
            self.here = True



    async def on_ready(self):
        print("Logged in " + self.name + " with \n" +
              "Name: " + self.UserBot.user.name + "\n" +
              "and ID: " + self.UserBot.user.id + "\n" +
              "------------------------------------------------")

    async def on_message(self, message):
        msg = {}
        if message.type == discord.MessageType.default and self.here:
            if message.author.id in self.userList.keys():
                msg = self.getRandomMessage(message.content, self.userList[message.author.id])
            else:
                msg = self.getRandomMessage(message.content, "any")
        return await self.sendMessage(message.channel, msg)



    @commands.command(pass_context=True)
    async def shutup(self, ctx: commands.Context): #makes the bot "AFK"
        msg = self.getRandomMessage("leave")
        self.here = False
        if not msg == "":
            await self.UserBot.send_message(ctx.message.channel, msg)
        return await self.UserBot.change_presence(status=discord.Status.dnd, afk=True)

    @commands.command(pass_context=True)
    async def come(self, ctx: commands.Context): #makes the bot return from "AFK"
        msg = self.getRandomMessage("return")
        self.here = True
        if not msg == "":
            await self.UserBot.send_message(ctx.message.channel, msg)
        return await self.UserBot.change_presence(status=discord.Status.online, afk=False)

    @commands.command(pass_context=True)
    async def reload(self, ctx: commands.Context): #makes the bot return from "AFK"
        msg = self.getRandomMessage("reload")
        self.here = False
        self.load(self.lookup)
        self.here = True
        if not msg == "":
            return await self.UserBot.send_message(ctx.message.channel, msg)
